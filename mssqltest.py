'''
src: https://docs.microsoft.com/en-us/sql/connect/python/pyodbc/
Setp 3: Proof of concept connecting to SQL using pyodbc

Adaptado para miturno.database.windows.net

'''
# Connect

import pyodbc 
# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance. Pruebas en servidor local [OK]
# server = 'myserver,port' # to specify an alternate port
server = 'tcp:miturno.database.windows.net' 
database = 'MiTurno' 
username = 'nxqms' 
password = 'Netux.12' 
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

# Run query
#Sample select query
print()
print("=====================")
print("MiTurno: Cédula Query")
print("=====================\n")

cursor.execute("SELECT @@version;")
row = cursor.fetchone()
while row:
#    print(row[0])
    print(row)
    row = cursor.fetchone()
'''
RESPONSE TO QUERY
=================
Microsoft SQL Azure (RTM) - 12.0.2000.8
	Apr  9 2020 16:39:55
	Copyright (C) 2019 Microsoft Corporation
'''
print("\n")
print("=============================================================")
print("\n")
# Run query from list of queries
#Sample select query
date_begin = '2020-05-06'
date_end = '2020-05-13'
Turn_ID = 'DF11F695-F558-4B39-ABF1-7E04D92FFB80'
Client_ID = 'C873F364-E86E-4B3E-9304-BB206CD832B9'

Query_Dyn = [
        "select FORMAT(cast(dateadd(HOUR, -5, T.CreationDate) as datetime), 'dd/MM/yyyy HH:MM') AS [Fecha Creación], " + "\n" + \
        "FV.TurnId AS [Turn Id], " + "\n" + \
	"T.TurnNumber AS [Número de Turno], " + "\n" + \
	"TY.Name AS [Tipo de Turno], " + "\n" + \
	"TY.Letter AS [Nomenclatura], " + "\n" + \
	"dbo.RemoveChars( FV.FormValue ) [Cédula], " + "\n" + \
	"C.Name AS Sede " + "\n" + \
	"from Turn T " + "\n" + \
	"LEFT OUTER JOIN TurnType TY on T.TurnType=TY.Id " + "\n" + \
	"LEFT OUTER JOIN FormValues FV on FV.TurnId = T.Id " + "\n" + \
	"LEFT OUTER JOIN Client C on c.Id=TY.ClientId " + "\n" + \
	" where --T.Id='" + Turn_ID + "' "+ "\n" + \
	" T.CreationDate between " + "\n" + \
	" '" +date_begin + "' and '"+date_end+"'  "+ "\n" + \
	" and C.Id='" + Client_ID + "'; " + '',
        "select * from ClientUsersDatabase;",
        "select * from Client;"]


print(Query_Dyn[0])

cursor.execute(Query_Dyn[2])
'''
cursor.execute("""
        select 
FORMAT(cast(dateadd(HOUR, -5, T.CreationDate) as datetime), 'dd/MM/yyyy HH:MM') AS [Fecha Creación],

FV.TurnId AS [Turn Id],
T.TurnNumber AS [Número de Turno],
TY.Name AS [Tipo de Turno],
TY.Letter AS [Nomenclatura],
dbo.RemoveChars( FV.FormValue ) [Cédula],
C.Name AS Sede
from Turn T
LEFT OUTER JOIN TurnType TY on T.TurnType=TY.Id
LEFT OUTER JOIN FormValues FV on FV.TurnId = T.Id
LEFT OUTER JOIN Client C on c.Id=TY.ClientId
 where --T.Id='DF11F695-F558-4B39-ABF1-7E04D92FFB80'
 T.CreationDate between 
 '2020-05-05' and '2020-05-06'  
 and C.Id='C873F364-E86E-4B3E-9304-BB206CD832B9';""")
'''

print("\n")
print("=============================================================")
print("\n")
row = cursor.fetchone()
while row:
#    print(row[0] + "|" + row[1])
    print(row)
    row = cursor.fetchone()

#
#Sample insert query
'''
cursor.execute("""
INSERT INTO SalesLT.Product (Name, ProductNumber, StandardCost, ListPrice, SellStartDate)
VALUES (?,?,?,?,?)""",
'SQL Server Express New 20', 'SQLEXPRESS New 20', 0, 0, CURRENT_TIMESTAMP)
cnxn.commit()
row = cursor.fetchone()

while row:
    print('Inserted Product key is ' + str(row[0]))
    row = cursor.fetchone()

'''
'''
Query_Dyn = [
        "select FORMAT(cast(dateadd(HOUR, -5, T.CreationDate) as datetime), 'dd/MM/yyyy HH:MM') AS [Fecha Creación], " + "\n" + \
	"FV.TurnId AS [Turn Id], " + "\n" + \
	"T.TurnNumber AS [Número de Turno], " + "\n" + \
	"TY.Name AS [Tipo de Turno], " + "\n" + \
	"TY.Letter AS [Nomenclatura], " + "\n" + \
	"dbo.RemoveChars( FV.FormValue ) [Cédula], " + "\n" + \
	"C.Name AS Sede " + "\n" + \
	"from Turn T " + "\n" + \
	"LEFT OUTER JOIN TurnType TY on T.TurnType=TY.Id " + "\n" + \
	"LEFT OUTER JOIN FormValues FV on FV.TurnId = T.Id " + "\n" + \
	"LEFT OUTER JOIN Client C on c.Id=TY.ClientId " + "\n" + \
	" where --T.Id='" + Turn_ID + "' "+ "\n" + \
	" T.CreationDate between " + "\n" + \
	" '" +date_begin + "' and '"+date_end+"'  "+ "\n" + \
	" and C.Id='" + Client_ID + "'; " + '',
        "select * from ClientUsersDatabase;"]

'''
