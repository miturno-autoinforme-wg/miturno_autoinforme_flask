select 
FORMAT(cast(dateadd(HOUR, -5, T.CreationDate) as datetime), 'dd/MM/yyyy HH:MM') AS [Fecha Creaci�n],

FV.TurnId AS [Turn Id],
T.TurnNumber AS [Número de Turno],
TY.Name AS [Tipo de Turno],
TY.Letter AS [Nomenclatura],
dbo.RemoveChars( FV.FormValue ) [Cédula],
C.Name AS Sede
from Turn T
LEFT OUTER JOIN TurnType TY on T.TurnType=TY.Id
LEFT OUTER JOIN FormValues FV on FV.TurnId = T.Id
LEFT OUTER JOIN Client C on c.Id=TY.ClientId
 where --T.Id='DF11F695-F558-4B39-ABF1-7E04D92FFB80'
 T.CreationDate between 
 '2020-05-01' and '2020-05-06'  
 and C.Id='C873F364-E86E-4B3E-9304-BB206CD832B9'
