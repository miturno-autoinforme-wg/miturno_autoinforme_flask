DECLARE @InitialDate datetime;
DECLARE @FinalDate datetime;
DECLARE @ClientId uniqueIdentifier;

--select * from client where NAME like ('%avida%')

SET @InitialDate = '2020-02-29'
SET @FinalDate = '2020-04-16'
SET @ClientId = 'C873F364-E86E-4B3E-9304-BB206CD832B9'



SELECT
	[TurnId],
	TurnType.Letter,
	Turn.TurnNumber,
	FORMAT(cast(dateadd(HOUR, -5, TurnCreationDate) as datetime), 'dd/MM/yyyy HH:MM') AS [Fecha Creaci�n],
	CONVERT(FLOAT, ROUND(TiempoEnSala,2,1)) AS TiempoEnSala,
	CONVERT(FLOAT, ROUND(TiempoEnTaquilla,2,1)) AS TiempoEnTaquilla,
	--CONCAT('Etapa ', TurnTypeStage.StepNumber )  AS Etapa,
	case when TurnTypeStage.IsDeleted!=1 then TurnTypeStage.StepNumber + 1 END AS [Etapa],
	COALESCE (AspNetUsers.FirstName + ' ', '') + COALESCE (AspNetUsers.MiddleName + ' ', '') + COALESCE (AspNetUsers.LastName + ' ', '') + COALESCE (AspNetUsers.SecondLastName, '') AS Nombre, 
	Reception.Name AS Taquilla
FROM (
	SELECT
		[TurnId],
		receptionId,
		StageId,
		TurnNumber,
		TurnCreationDate,
		UserId,
		TurnTypeId,
		datediff(second, [6], [3])/ 60.0 as TiempoEnSala,
		datediff(second, [3], [1])/ 60.0 as TiempoEnTaquilla
	FROM (
		SELECT DISTINCT 
			Query.[TurnId],
			Query.receptionId,
			Query.StageId,
			MIN(TurnNumber) as TurnNumber,
			MIN(TurnCreationDate) as TurnCreationDate,
			MIN(Query.UserId) as UserId, 
			MIN(Query.TurnTypeId) as TurnTypeId,
			MIN([6]) as [6],
			MIN([3]) as [3],
			min([1]) as [1]
			FROM (
				SELECT 
					[TurnId],
					StageId,
					receptionId,
					ss, MIN(TurnNumber) as TurnNumber,
					MIN(TurnCreationDate) as TurnCreationDate,
					MIN(UserId) as UserId,
					MIN(TurnTypeId) as TurnTypeId,
					MIN([6]) as [6],
					MIN([3]) as [3],
					MIN([1]) as [1]
				FROM
				( 
				  SELECT
					  Turn.[Id] as TurnId
					  ,Turn.CreationDate as TurnCreationDate
					  ,TurnNumber
					  ,TurnType.Id as TurnTypeId
					  ,UserId
					  ,ReceptionId
					   ,[TurnStatus] as SS
					  ,[TurnStatus]
					  ,[TurnStates].[CreationDate]
					  ,[Stage] as StageId
	  

				  FROM [dbo].Turn

				  INNER JOIN [TurnStates] on  Turn.id = [TurnStates].TurnId
				  INNER JOIN TurnType on Turn.TurnType = TurnType.id
				  INNER JOIN TurnAttention on TurnAttention.TurnId = Turn.id
				  WHERE 
						([TurnStatus] = 3 or [TurnStatus] = 1  or [TurnStatus] = 6) AND 
						[TurnStates].[CreationDate] >= @InitialDate  AND  [TurnStates].[CreationDate] <= @FinalDate AND
						TurnType.ClientId = @clientId
						--AND Turn.Id = 'ffe7586a-1266-4937-afa2-5d345a8ecd4d'
				) AS datasource
  
				PIVOT (
					MIN([CreationDate])
					For TurnStatus in ( [1] , [3] , [6])
				) AS pvt

				GROUP BY [TurnId], StageId, receptionId, ss
			) AS Query

		INNER JOIN TurnAttention on TurnAttention.TurnId = Query.TurnId
		WHERE TurnAttention.TurnId = Query.TurnId and TurnAttention.StageId = Query.StageId and TurnAttention.receptionId = Query.receptionId
		GROUP BY Query.[TurnId], Query.StageId, Query.receptionId --,[6], [3], [1]

		) AS MyTable
		WHERE [1] is not null and [3] is not null and [3] is not null
		--GROUP BY UserId, TurnTypeId, ReceptionId, StageId
	
	) AS myQuery 
	INNER JOIN Reception ON Reception.Id = myQuery.ReceptionId
	INNER JOIN Turn ON Turn.Id = myQuery.TurnId
	INNER JOIN TurnTypeStage ON TurnTypeStage.TurnTypeId = myQuery.TurnTypeId
	INNER JOIN TurnType ON TurnType.Id = myQuery.TurnTypeId
	INNER JOIN AspNetUsers ON AspNetUsers.Id = myQuery.UserId
	ORDER BY TurnCreationDate
option(recompile)