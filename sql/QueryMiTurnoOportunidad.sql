/****** Object:  StoredProcedure [dbo].[OportunityReport]    Script Date: 15/07/2019 1:17:52 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- OPORTUNIDAD

DECLARE @InitialDate datetime;
DECLARE @FinalDate datetime;
DECLARE @ClientId uniqueIdentifier;

SET @InitialDate = '2020-02-29'
SET @FinalDate = '2020-04-15'
SET @ClientId = 'C873F364-E86E-4B3E-9304-BB206CD832B9'
	
	
SELECT 
	COALESCE (AspNetUsers.FirstName + ' ', '') + COALESCE (AspNetUsers.MiddleName + ' ', '') + COALESCE (AspNetUsers.LastName + ' ', '') + COALESCE (AspNetUsers.SecondLastName, '') AS Nombre, 
    Reception.Name AS Taquilla, 
	TurnType.Name AS TipoTurno, 
	--CONCAT ('Etapa ', TurnTypeStage.StepNumber + 1) AS Etapa, 
	case when TurnTypeStage.StepNumber!=1 then TurnTypeStage.StepNumber + 1 END AS [Etapa Sola],
	NumeroDeAtenciones, 
	CONVERT(FLOAT, ROUND(EnAtencion,2,1)) AS EnAtencion
	FROM (
		SELECT
			receptionId,
			StageId,
			UserId,
			TurnTypeId,
			count([TurnId]) as NumeroDeAtenciones,
			AVG(datediff(second, [6], [3]))/ 60.0 as EnSala,
			AVG(datediff(second, [3], [1]))/ 60.0 as EnAtencion
		FROM (
			SELECT DISTINCT 
				Query.[TurnId],
				Query.receptionId,
				Query.StageId, 
				MIN(TurnNumber) as TurnNumber,
				MIN(TurnCreationDate) as TurnCreationDate,
				MIN(Query.UserId) as UserId,
				MIN(Query.TurnTypeId) as TurnTypeId,
				MIN([6]) as [6], MIN([3]) as [3], MIN([1]) as [1]  
				from (
					SELECT 
						[TurnId] 
						,StageId
						,receptionId
						,ss
						,MIN(TurnNumber) as TurnNumber
						,MIN(TurnCreationDate) as TurnCreationDate
						,MIN(UserId) as UserId
						,MIN(TurnTypeId)  as TurnTypeId
						,MIN([6]) as [6]
						,MIN([3]) as [3]
						,MIN([1]) as [1]  
					FROM
					( 
					  SELECT
						  Turn.[Id] as TurnId
						  ,Turn.CreationDate as TurnCreationDate
						  ,TurnNumber
						  ,TurnType.Id as TurnTypeId
						  ,UserId
						  ,ReceptionId
						  ,[TurnStatus] as SS
						  ,[TurnStatus]
						  ,[TurnStates].[CreationDate]
						  ,[Stage] as StageId
					  FROM [dbo].Turn

					  INNER JOIN [TurnStates] on Turn.id = [TurnStates].TurnId
					  INNER JOIN TurnType on Turn.TurnType = TurnType.id
					  INNER JOIN TurnAttention on TurnAttention.TurnId = Turn.id
	  
					  WHERE 
							([TurnStatus] = 3 or [TurnStatus] = 1  or [TurnStatus] = 6) AND
							[TurnStates].[CreationDate] >= @InitialDate  AND  [TurnStates].[CreationDate] <= @FinalDate  AND 
							TurnType.ClientId = @clientId
					) AS datasource
	  
					PIVOT (  
						MIN([CreationDate])
						For TurnStatus in ([1] , [3] , [6])
					) as pvt

	  
					GROUP BY [TurnId], StageId, receptionId,  ss

				) AS Query

				INNER JOIN TurnAttention on TurnAttention.TurnId = Query.TurnId
				WHERE TurnAttention.TurnId = Query.TurnId and TurnAttention.StageId = Query.StageId and TurnAttention.receptionId = Query.receptionId
				GROUP BY Query.[TurnId] , Query.StageId, Query.receptionId--,[6], [3], [1]

			) AS MyTable
			
	 
		GROUP BY UserId, TurnTypeId, ReceptionId, StageId
		
	) AS myQuery 
	INNER JOIN Reception ON Reception.Id = myQuery.ReceptionId
	INNER JOIN TurnTypeStage ON TurnTypeStage.TurnTypeId = myQuery.TurnTypeId
	INNER JOIN TurnType ON TurnType.Id = myQuery.TurnTypeId
	INNER JOIN AspNetUsers ON AspNetUsers.Id = myQuery.UserId
option(recompile)




