# MiTurno AutoInformes #

Generación de automática de **informes privados** para clientes de  **MiTurno**, partiendo de los **queries y métodos de depuración ya desarrollados por Santiago Alzate y Laura Moreno**

### Insumos ###

* SQL de los cuerys ya probados
* Muestras de reportes a entregar
* Acuerdo en usar Python como lenguage de desarrollo
### ¿Qué estamos desarrollando? ###
1.  Acceso a la base de datos [SQL-Server](https://miturno.database.windows.net) usando los tres querys probados desde Python

	* MiTurnoAtencion
	* MiTurnoOportunidad
	* MiTurnoPorCedula	

1. Generación diaria de manera automática de los reportes luego de la medianoche, (hora colombiana) que incluyen todos los eventos con fecha de inicio en el día del reporte, analilzándolos aunque se extiendan más allá del día de generación 
1. Acceso controlado para cada cliente al área específica de sus informes
1. Login

    * Registro
    * Recuperación de clave

1. Ver listado de reportes disponibles por cliente

    * Filtrar por Rangho de Fechas

1. Selección por el cliente de Rango de Fechas de los informes a descargar

    * Consulta del informe generado en pantalla
    * Descarga de los informes seleccionados en formato CSV

### Esquema de la aplicación ##
![Propuesta inicial de MiTurno AutoInforme](https://bitbucket.org/miturno-autoinforme-wg/miturno_autoinforme/raw/34489ae3bbb5ea3c933c9c285432a319869c9c37/doc_images/MiTurno_App_Outline.png "Propuesta inicial de MiTurno AutoInforme")

### Evaluación de plataformas de desarrollo para Python
Para el proyecto evalué estas plataformas:

1. [Django](https://www.djangoproject.com/) Es un framework de alto nivel desde su concepción
1. [TurboGears](https://turbogears.org/) Es un framework evolutivo desde un microframework hasta un frame de alto nivel. Usa MongoDB como base
1. [Plone](https://plone.org/) Lo he usado antes, es sumamente maduro, estable y poderoso, pero está orientado a soluciones mucho más grandes
1. [Flask](https://flask.palletsprojects.com/en/1.1.x/). Lo denominan microframework, está oprientado a soluciones simples

Aunque Django es el más difundido y con gran soporte, su curva de aprendizaje se presentó más compleja en un principio. Se usa en aplicaciones profesionales de manera muy amplia y es la preferida para proyectos escalables. Inicialmente se construye un proyecto al cual se agregan aplicaciones específicas.

TurboGears es muy intersante por su caracter evolutivo desde microframework hasta framework "fullstack" pude seguir sin  problema los ejemplos propuestos para empezar, pero realmente es extenso luego de la instalación

Plone es demasiado elaborado para la solución que se requiere. Lo volví a instalar y a evaluar, ha crecido y se ha desarrollado muchísmo. Demasiado para lo que necesitamos

Para implementar la solución estoy usando Flask es menos sobrecargado pero permite gran estructuración en el diseño, me voy a apoyar en estos componentes en particular:

1. [Blueprints](https://flask.palletsprojects.com/en/1.1.x/blueprints/): Permite un desarrollo sumamentei simple, modular y reutilizable
1. [Flask-WTForms](https://flask-wtf.readthedocs.io/en/stable/) Ingra Flask con WTForms de Python
1. [SQLAlchemy](https://www.sqlalchemy.org/). Genérico de Python, pero normaliza el uso de bases de datosi
1. [Flask-WTForms](https://flask-wtf.readthedocs.io/en/stable/) Ingra Flask con WTForms de Python
1. [lxml](https://lxml.de/) Permite procesar XML/XSLT en Python para generar XML/texto/HTML


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Equipo: ###

* Rubén Tabares
* Antonio Gonzáles
* Laura Moreno
* Santiago Alzate
* Hugo Ángel
